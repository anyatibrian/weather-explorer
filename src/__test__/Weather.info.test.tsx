import {render, screen, fireEvent, waitFor} from '@testing-library/react';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import WeatherInfo from '../pages/WeatherInfo';
const mock = new MockAdapter(axios);
//handle mocking of jest request and
test('Weather Info Title with Sub', async () => {
    render(<WeatherInfo />);
    const introText = await screen.findByText('Tody Weather Focus', {
        exact: false,
    });
    const subHeading = await screen.findByText(
        'your current city weather information is just a browser away',
        {exact: false}
    );
    expect(subHeading).toBeInTheDocument();
    expect(introText).toBeInTheDocument();
});
test('Render back and prev Icons', async () => {
    render(<WeatherInfo />);
    const altImageNextIcon = await screen.findByAltText(/nextIcon/i);
    const altImageprevIcon = await screen.findByAltText(/nextIco/i);
    expect(altImageNextIcon).toBeTruthy();
    expect(altImageprevIcon).toBeTruthy();
});

test('click back and prev Icons', async () => {
    const handlePrevClick = jest.fn();
    render(<WeatherInfo />);
    const altImagePrevIcon = await screen.findByTestId('click-prev-button');
    const listCities = screen.findByTestId('list-cities');
    expect(altImagePrevIcon).toBeInTheDocument();
    fireEvent.click(altImagePrevIcon);
    expect(listCities).toBeTruthy;
});
