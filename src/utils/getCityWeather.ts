import axios from 'axios';
import {instance} from './axiosInstance';
/**
 * Get city weather information
 * @param lat
 * @param lon
 * @returns
 */
export const getCityWeatherInfo = async (lat: string, lon: string) => {
    const {data} = await instance({
        url: `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=da374e1e0efa46d6fac278390fe7e968&units=metric`,
        method: 'GET',
    });
    return data;
};
