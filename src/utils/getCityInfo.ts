import axios from 'axios';
import {instance} from './axiosInstance';
/**
 * Todos:
 * create a function that accepts city locations
 * the fuction makes queries to mapbox geocording api and
 * return various cities and their location detailed information
 */
/**
 * Get verious city location using the map Geocording API
 * linkk https://docs.mapbox.com/api/search/geocoding/
 * @param city
 * @returns
 */
export const getCityInfo = async (city: string): Promise<any> => {
    const result = await instance({
        url: `https://api.mapbox.com/geocoding/v5/mapbox.places/${city}.json?proximity=-74.70850,40.78375&access_token=pk.eyJ1IjoiYW55YXRpYnJpYW4iLCJhIjoiY2s1cGdrN3Y2MHNqbjNobW80eXF2MHAwNyJ9.3valCGJERUuqC_EqTY-E1Q&limit=1`,
        method: 'GET',
    });

    return result.data;
};
