export const fahrenheitToCelsius = (fahrenheit: any) => {
    return ((fahrenheit - 32) * 5) / 9;
};
