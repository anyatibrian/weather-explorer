import {WICard} from '../components/WICard';
import Cities from '../cities.json';
import {useEffect, useState} from 'react';
import {getCityInfo} from '../utils/getCityInfo';
import {getCityWeatherInfo} from '../utils/getCityWeather';
import nextIcon from '../resources/nextIcon.svg';
const WeatherInfo = () => {
    //items per page
    const itemsPerPage = 3;
    const [cityDetail, setCityDetail] = useState<any[]>([]);
    const [currentPage, setCurrentPage] = useState<number>(1);
    const [loading, setLoading] = useState(false);
    //list all the cities with latitude and weather Information
    useEffect(() => {
        (async () => {
            const result = Cities.map(async item => {
                //get city detail information
                setLoading(true);
                const cityInfo = await getCityInfo(item.name);
                const lat = cityInfo['features'][0]['center'][1];
                const long = cityInfo['features'][0]['center'][0];
                //get respective weather information
                const cityWeatherInfo = await getCityWeatherInfo(lat, long);
                setLoading(false);
                //returning city detail information
                return {
                    ...item,
                    coordinates: cityInfo['features'][0]['center'],
                    placeName: cityInfo['features'][0]['place_name'],
                    context: cityInfo['features'][0]['context'],
                    weather: cityWeatherInfo,
                };
            });
            const results = await Promise.all(result);
            setCityDetail(results);
        })();
    }, []);
    //[begin]:get the start index and end index
    const startIndex = (currentPage - 1) * itemsPerPage;
    const endIndex = startIndex + itemsPerPage;
    //[endq]:get the start index and end index
    const handleNextPage = () => {
        setCurrentPage(prevPage => prevPage + 1);
    };
    const handlePrevPage = () => {
        setCurrentPage(prevPage => Math.max(prevPage - 1, 1));
    };
    const currentCityPerPage = cityDetail.slice(startIndex, endIndex);
    const listCities = currentCityPerPage.map((item, index) => {
        return (
            <WICard
                name={item.name}
                country={item.country}
                temp={item.weather.main.temp}
                humidity={item.weather.main.humidity}
                windSpeed={item.weather.wind.speed}
                windDirection={item.weather.wind.deg}
                clouds={item.weather.clouds.all}
                weather={item.weather.weather}
                lat={item.coordinates[1]}
                long={item.coordinates[0]}
                key={Math.random() * index}
            />
        );
    });
    return (
        <div className="">
            <div className=" pb-5">
                <h1 className="text-center font-bold text-3xl pt-10 pb-1">
                    Tody Weather Focus
                </h1>
                <p className=" text-center font-extralight">
                    your current city weather information is just a browser away
                </p>
            </div>
            <div className="bg-slate-900 flex flex-row pt-10 pb-10">
                {!loading && (
                    <div
                        className="w-44 flex-row"
                        onClick={() => handlePrevPage()}
                        data-testid="click-prev-button"
                    >
                        {' '}
                        <img
                            src={nextIcon}
                            alt="prevIcon"
                            className=" translate-y-48 text-end rotate-180 border p-2 rounded-full ml-24"
                        />
                    </div>
                )}
                <div className="flex pb-10 pt-10" data-testid="list-cities">
                    {!loading && Cities.length > 0 && listCities}
                    {loading && (
                        <p className=" text-white text-xl mr-10 ml-10">
                            loading weather data...
                        </p>
                    )}
                </div>

                {!loading && (
                    <div className="" onClick={() => handleNextPage()}>
                        <img
                            src={nextIcon}
                            alt="nextIcon"
                            className="translate-y-48 border rounded-full p-2 ml-2"
                        />
                    </div>
                )}
            </div>
        </div>
    );
};
export default WeatherInfo;
