import windIcon from '../resources/windIcon.svg';
import humidityIcon from '../resources/humidity.svg';
import cloudIcon from '../resources/cloudIcon.svg';
import markerIcon from '../resources/marker.svg';
type CityProps = {
    name?: string;
    country?: string;
    temp?: any;
    humidity?: string;
    windSpeed?: string;
    windDirection?: string;
    weather?: any;
    clouds?: string;
    lat?: string;
    long?: string;
};
export const WICard = ({
    name,
    country,
    humidity,
    windSpeed,
    temp,
    windDirection,
    clouds,
    lat,
    long,
    weather,
}: CityProps) => {
    const _temp: any = Math.floor(temp);
    return (
        <div>
            <div className="flex-col flex-auto m-2 p-5 border rounded-md bg-white w-80 mr-3 ml-3">
                <div className=" flex flex-row">
                    <h2 className=" text-black text-lg">{name}</h2>
                    <p className="bg-yellow-600 text-xs rounded-lg text-white font-bold pr-2 pl-2 pt-1 mr-2">
                        {country}
                    </p>
                </div>
                <div className=" flex flex-row pt-5 pb-2">
                    <h2 className=" text-5xl">{_temp}</h2>
                    <h2 className=" mt-1">0</h2>
                    <h2 className=" text-5xl mt-1">C</h2>
                </div>
                <div className=" flex flex-row mt-2 mb-2">
                    <div className="">
                        <img src={markerIcon} alt="marker Icon" />
                    </div>
                    <div>
                        <p className="ml-1">Current Location</p>
                        <p className=" text-xs ml-2">lat:{lat}</p>
                        <p className="text-xs ml-2">long:{long}</p>
                    </div>
                </div>
                <div>
                    <p className="ml-1 font-extralight text-lg">
                        {weather[0]['description']}
                    </p>
                </div>
                <div className="flex flex-row">
                    <div className=" bg-slate-900 p-4 rounded-md m-1">
                        <div>
                            <img src={cloudIcon} />
                        </div>
                        <div className="pt-2 pb-2">
                            <p className=" text-white text-xs text-center">
                                {clouds}
                            </p>
                            <p className=" text-white text-xs text-center">
                                Clouds
                            </p>
                        </div>
                    </div>
                    <div className=" bg-slate-900 p-4 rounded-md m-1">
                        <div>
                            <img src={humidityIcon} alt="humidityIcon" />
                        </div>
                        <div className=" pt-2 pb-2">
                            <p className=" text-white pl-4 text-xs">
                                {humidity}
                            </p>
                            <p className=" text-white pl-0 text-xs">humidity</p>
                        </div>
                    </div>
                    <div className="bg-slate-900 rounded-md p-4 m-1">
                        <div className="">
                            <img
                                src={windIcon}
                                alt="windIcon"
                                className=" pl-3"
                            />
                        </div>
                        <div className=" pt-2 pb-2">
                            <p className=" text-white text-xs text-center">
                                {windSpeed}m/s
                            </p>
                            <p className=" text-white text-xs"> WindSpeed</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
